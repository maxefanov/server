# -*- coding: utf-8 -*-

import os.path
import base64
import json
import lxml
import requests
import werkzeug.exceptions
import werkzeug.urls
import werkzeug.wrappers

from datetime import datetime

from odoo import fields, http, tools, modules, SUPERUSER_ID, _
from odoo.http import content_disposition, Controller, request, route
from odoo.addons.web.controllers.main import binary_content
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager  # Import the classes

from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website.controllers.main import Website
from odoo.addons.sale.controllers.product_configurator import ProductConfiguratorController
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.osv import expression
from odoo.addons.website_sale.controllers.main import WebsiteSale  # Import the class
from odoo.addons.website_sale.controllers.main import TableCompute  # Import the class

from odoo.addons.website.controllers.main import Website  # Import the class

from odoo.addons.l10n_ru_doc.report_helper import QWebHelper
from odoo.addons.portal.controllers.mail import _message_post_helper

import logging
_logger = logging.getLogger(__name__)

class MomaCustomerPortal(CustomerPortal):

    MANDATORY_BILLING_FIELDS = ["name", "email"]
    OPTIONAL_BILLING_FIELDS = ["phone", "web_site", "customer", "supplier", "country_id", "state_id", "street", "city", "zipcode", "partner_type", "is_company", "company_name", "inn", "kpp", "okpo"]

    @route(['/my/account'], type='http', auth='user', website=True)
    def account(self, redirect=None, **post):
        if post:
            # _logger.info( "/my/account) post=%s" % post )
            if( post['partner_type']=="4" ):
                post['is_company'] = 'on'
            else:
                post['is_company'] = ''
            # _logger.info( "!!!2) post=%s" % post )
            if not post['country_id'] :
                countries_ru = request.env['res.country'].search( [ ('code','=','RU') ] )
                post['country_id'] = countries_ru[0].id # Россия
        res = super(MomaCustomerPortal, self).account(redirect,**post)
        return res

    @http.route(['/my/orders/<int:order_id>'], type='http', auth="public", website=True)
    def portal_order_page(self, order_id, report_type=None, access_token=None, message=False, download=False, **kw):
        try:
            order_sudo = self._document_check_access('sale.order', order_id, access_token=access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        if report_type in ('html', 'pdf', 'text'):
            return self._show_report(model=order_sudo, report_type=report_type, report_ref='sale.action_report_saleorder', download=download)

        # use sudo to allow accessing/viewing orders for public user
        # only if he knows the private token
        now = fields.Date.today()

        # Log only once a day
        if order_sudo and request.session.get('view_quote_%s' % order_sudo.id) != now and request.env.user.share and access_token:
            request.session['view_quote_%s' % order_sudo.id] = now
            body = _('Quotation viewed by customer')
            _message_post_helper(res_model='sale.order', res_id=order_sudo.id, message=body, token=order_sudo.access_token, message_type='notification', subtype="mail.mt_note", partner_ids=order_sudo.user_id.sudo().partner_id.ids)

        values = {
            'helper': QWebHelper(),
            'sale_order': order_sudo,
            'message': message,
            'token': access_token,
            'return_url': '/shop/payment/validate',
            'bootstrap_formatting': True,
            'partner_id': order_sudo.partner_id.id,
            'report_type': 'html',
        }
        if order_sudo.company_id:
            values['res_company'] = order_sudo.company_id

        if order_sudo.has_to_be_paid():
            domain = expression.AND([
                ['&', ('website_published', '=', True), ('company_id', '=', order_sudo.company_id.id)],
                ['|', ('specific_countries', '=', False), ('country_ids', 'in', [order_sudo.partner_id.country_id.id])]
            ])
            acquirers = request.env['payment.acquirer'].sudo().search(domain)

            values['acquirers'] = acquirers.filtered(lambda acq: (acq.payment_flow == 'form' and acq.view_template_id) or
                                                     (acq.payment_flow == 's2s' and acq.registration_view_template_id))
            values['pms'] = request.env['payment.token'].search(
                [('partner_id', '=', order_sudo.partner_id.id),
                ('acquirer_id', 'in', acquirers.filtered(lambda acq: acq.payment_flow == 's2s').ids)])

        if order_sudo.state in ('draft', 'sent', 'cancel'):
            history = request.session.get('my_quotations_history', [])
        else:
            history = request.session.get('my_orders_history', [])
        values.update( get_records_pager(history, order_sudo) )

        return request.render('sale.sale_order_portal_template', values)
