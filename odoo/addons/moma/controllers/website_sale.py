# -*- coding: utf-8 -*-

import os.path
import base64
import json
import lxml
import requests
import werkzeug.exceptions
import werkzeug.urls
import werkzeug.wrappers

from datetime import datetime

from odoo import fields, http, tools, modules, SUPERUSER_ID, _
from odoo.http import content_disposition, Controller, request, route
from odoo.addons.web.controllers.main import binary_content
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager  # Import the classes

from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import UserError,ValidationError
from odoo.addons.website.controllers.main import Website
from odoo.addons.sale.controllers.product_configurator import ProductConfiguratorController
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.osv import expression
from odoo.addons.website_sale.controllers.main import WebsiteSale  # Import the class
from odoo.addons.website_sale.controllers.main import TableCompute  # Import the class

from odoo.addons.website.controllers.main import Website  # Import the class

from odoo.addons.l10n_ru_doc.report_helper import QWebHelper

from psycopg2 import IntegrityError

import logging
_logger = logging.getLogger(__name__)

PPG = 20  # Products Per Page
PPR = 4   # Products Per Row

class MomaWebsiteSale(WebsiteSale):

    def _get_search_domain(self, search, category, attrib_values):
        domain = super(MomaWebsiteSale, self)._get_search_domain( search, category, attrib_values)

        domain += [('website_published', '=', True)]

        return domain

    def _get_search_domain_own_modules(self, search, category, attrib_values):
        domain = request.website.sale_product_domain()

        partner = request.env.user.partner_id
        domain += [('seller_id','=',partner.id)]

        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]

        if category:
            domain += [('', 'child_of', int(category))]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        return domain

    @http.route([ '''/my/own_modules''','''/my/own_modules/page/<int:page>''' ], type='http', auth="user", website=True)
    def own_modules(self, page=0, search='', ppg=False, **post):
        category=None
        add_qty = int(post.get('add_qty', 1))

        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        attrib_set = {v[1] for v in attrib_values}

        domain = self._get_search_domain_own_modules(search, category, attrib_values)

        keep = QueryURL('/my/own_modules', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))

        pricelist_context, pricelist = self._get_pricelist_context()

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)

        url = "/my/own_modules"
        if search:
            post["search"] = search
        if attrib_list:
            post['attrib'] = attrib_list

        Product = request.env['product.template'].with_context(bin_size=True)

        #Category = request.env['product.public.category']
        search_categories = False
        search_product = Product.search(domain)
        #if search:
        #    categories = search_product.mapped('public_categ_ids')
        #    search_categories = Category.search([('id', 'parent_of', categories.ids)] + request.website.website_domain())
        #    categs = search_categories.filtered(lambda c: not c.parent_id)
        #else:
        #    categs = Category.search([('parent_id', '=', False)] + request.website.website_domain())

        #parent_category_ids = []
        #if category:
        #    url = "/shop/category/%s" % slug(category)
        #    parent_category_ids = [category.id]
        #    current_category = category
        #    while current_category.parent_id:
        #        parent_category_ids.append(current_category.parent_id.id)
        #        current_category = current_category.parent_id

        product_count = len(search_product)
        pager = request.website.pager(url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
        products = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))

        ProductAttribute = request.env['product.attribute']
        if products:
            # get all products without limit
            attributes = ProductAttribute.search([('attribute_line_ids.value_ids', '!=', False), ('attribute_line_ids.product_tmpl_id', 'in', search_product.ids)])        
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        compute_currency = self._get_compute_currency(pricelist, products[:1])

        values = {
            'search': search,
            #'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            #'add_qty': add_qty,
            'products': products,
            'search_count': product_count,  # common for all searchbox
            'bins': TableCompute().process(products, ppg),
            'rows': PPR,
            #'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            #'parent_category_ids': parent_category_ids,
            #'search_categories_ids': search_categories and search_categories.ids,
        }
        #if category:
        #    values['main_object'] = category 
        return request.render("moma.own_modules", values)

    def details_form_validate(self, data,mandatory_fields,optional_fields):
        error = dict()
        error_message = []

        # Validation
        for field_name in mandatory_fields:
            if not data.get(field_name):
                error[field_name] = 'отсутствует'

        if int(data.get('mod_class'))<1:
            error["mod_class"] = 'error'
            error_message.append(_('Укажите вид модуля.'))

        if int(data.get('mod_type'))<1:
            error["mod_type"] = 'error'
            error_message.append(_('Укажите тип модуля.'))

        # error message for empty required fields
        if [err for err in error.values() if err == 'отсутствует']:
            error_message.append(_('Некоторые поля не заполнены.'))

        #unknown = [k for k in data if k not in mandatory_fields + optional_fields]
        #if unknown:
        #    error['common'] = 'Поле неизвестно'
        #    error_message.append("Неизвестно поле '%s'" % ','.join(unknown))

        #error['common'] = 'Отладка'
        #error_message.append("pub_cats = '%s'" % ','.join(data.get("pub_cats")))
        #Укажите тип модуля. 
        #pub_cats = '8,,,7,,,9,,,3' 

        return error, error_message

    @http.route([ '''/my/versions4own_module/product/<model("product.template"):product>''' ], type='http', auth="user", website=True)
    def versions4own_module(self, product=None, **post):
        values = { 'error': {}, 'error_0': {}, 'error_message': [], 'error_message_0': [], 'err4att': 0 }

        if product:
            attachments = request.env['ir.attachment'].search( [('res_model', '=', 'product.template'), ('res_id', '=', product.id), ('name', '=', 'module') ], order="create_date" )
            values.update({
                'product': product,
                'attachments': attachments,
            })

        if post:
            error = {}
            error_message = []
            _logger.info( "!!! versions4own_module POST %s" % post )
            partner = request.env.user.partner_id
            _logger.info( "!!! product.seller_id=%s" % product.seller_id )
            _logger.info( "!!! partner=%s" % partner )
            if product.seller_id==partner:
                ir_att = request.env['ir.attachment'].browse( int(post.get("attachment_id")) )
                dict4write = dict( version = post.get('version'),
                                   is_published = post.get('is_published'),
                                   version_description = post.get('version_description') )
                if post.get('upload_cfe_file'):
                    # file_name = post.get('upload_cfe_file').filename
                    cfe_file = post.get('upload_cfe_file').read()
                    dict4write['datas'] = base64.b64encode(cfe_file)
                try:
                    ir_att.sudo().write( dict4write )
                    _logger.info( "!!! ir_att write no problem" )
                except UserError as e:
                    err_msg = e.name or e.value
                    if 'версия' in err_msg.lower():
                        error['version'] = err_msg
                        error_message.append( err_msg )
                    else:
                        error_message.append( err_msg )
                    values.update( {'err4att': int(post.get("attachment_id")), 'error': error, 'error_message': error_message} )
                    values.update( {'version_err': post.get('version'), 'is_published_err': post.get('is_published'), 'version_description_err': post.get('version_description')} )
                    _logger.info( "!!! before render values=%s" % values )
                    response = request.render("moma.my_versions4own_module", values)
                    response.headers['X-Frame-Options'] = 'DENY'
                    return response

        response = request.render("moma.my_versions4own_module", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    def _complete_name_for_pubcat(self,pubcat):
        if pubcat.parent_id:
            return '%s / %s' % (self._complete_name_for_pubcat(pubcat.parent_id), pubcat.name)
        else:
            return pubcat.name

    @http.route([
        '''/my/own_modules/product''',
        '''/my/own_modules/product/<model("product.template"):product>'''
    ], type='http', auth="user", website=True)
    def own_module(self, product=None, **post):
        mandatory_fields = ["name", "tech_name", "mod_class", "mod_type"]
        optional_fields = ["description", "list_price", "attribute_line_ids", "website_published", "seller_id", "blog_post_id","version","version_description"]

        partner = request.env.user.partner_id
        values = { 'type': 'service',
                'seller_id': partner.id,
                'error': {},
                'error_message': [],
                }

        if post:
            error, error_message = self.details_form_validate(post,mandatory_fields,optional_fields)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)
            if not error:
                values = {key: post[key] for key in mandatory_fields}
                values.update({key: post[key] for key in optional_fields if key in post})
                #values.update({'zip': values.pop('zipcode', '')})
                if 'clear_image' in post:
                    values['image'] = False
                elif post.get('upload_image_file'):
                    image = post.get('upload_image_file').read()
                    values['image'] = base64.b64encode(image)
                if 'pub_cats' in post:
                    tags = post.get('pub_cats')
                    #PubCat = request.env['product.public.category']
                    post_tags = []
                    existing_keep = []
                    for tag in (tag for tag in tags.split(',') if tag):
                        if not tag.startswith('_'):  # it's not a new tag
                            existing_keep.append(int(tag))
                    post_tags.insert(0, [6, 0, existing_keep])
                    values['public_categ_ids'] = post_tags
                if product and 'for_confs' in post:
                    for_confs = post.get('for_confs')
                    _logger.info( "!!! for_confs=%s" % for_confs )
                    post_attrs = []
                    existing_keep = []
                    for tag in (tag for tag in for_confs.split(',') if tag):
                        if not tag.startswith('_'):  # it's not a new tag
                            existing_keep.append(int(tag))
                    prod_attr = request.env['product.attribute'].search( [('name', '=', 'Для конфигурации')] , limit=1 )
                    if len(existing_keep)==0 :
                        recs = request.env['product.template.attribute.line'].search( [('product_tmpl_id', '=', product.id), ('attribute_id','=',prod_attr[0].id) ] )
                        recs.unlink()
                    else :
                        recs = request.env['product.template.attribute.line'].search( [('product_tmpl_id', '=', product.id), ('attribute_id','=',prod_attr[0].id) ] , limit=1 )
                        if recs :
                            ptal = request.env['product.template.attribute.line'].browse(recs[0].id)
                            ptal.write({
                                'value_ids': [[6, False, existing_keep]],
                            })
                        else :
                            request.env['product.template.attribute.line'].create({
                                'product_tmpl_id': product.id,
                                'attribute_id': prod_attr[0].id,
                                'value_ids': [[6, False, existing_keep]],
                            })
                redir = '/my/own_modules'
                if product:
                    tech_name = values['tech_name']
                    del values['tech_name'] # нельзя изменять tech_name
                    prod_templ = request.env['product.template'].browse(product.id)
                    prod_templ.sudo().write( values )
                    values['tech_name'] = tech_name
                else:
                    
                    try:
                        with http.request.env.cr.savepoint():
                            product = request.env['product.template'].sudo().create( values )
                            redir = "/my/own_modules/product/"+str(product.id)
                    except IntegrityError as ie:
                        _logger.info( "!!! IntegrityError=%s" % ie )
                        err_msg = str(ie)
                        if 'tech_name' in err_msg.lower():
                            error['tech_name'] = err_msg
                        error_message.append( err_msg )
                        values.update({'error': error, 'error_message': error_message})
                        response = request.render("moma.my_own_module", values)
                        response.headers['X-Frame-Options'] = 'DENY'
                        return response

                    
                if 'priv_partners' in post:
                    tags = post.get('priv_partners')
                    existing_keep = []
                    for tag in (tag for tag in tags.split(',') if tag):
                        if not tag.startswith('_'):  # it's not a new tag
                            existing_keep.append(int(tag))
                    mmp2del = request.env['moma.module_privated'].search( [('module_id', '=', product.id), ('buyer_id','not in',existing_keep) ] )
                    mmp2del.unlink()
                    mmp_exist = request.env['moma.module_privated'].search( [('module_id', '=', product.id), ('buyer_id','in',existing_keep) ] )
                    #_logger.info( "!!! mmp_exist=%s" % mmp_exist )
                    #_logger.info( "!!! mmp_exist.buyer_id.ids=%s" % mmp_exist.buyer_id.ids )
                    for partner_id in existing_keep:
                        if partner_id not in mmp_exist.buyer_id.ids:
                            request.env['moma.module_privated'].create({
                                'module_id': product.id,
                                'buyer_id': partner_id,
                            })
                if post.get('upload_cfe_file'):
                    file_name = post.get('upload_cfe_file').filename
                    file_ext = os.path.splitext(file_name)[1][1:]
                    cfe_file = post.get('upload_cfe_file').read()
                    #recs = request.env['ir.attachment'].search( [('res_model', '=', 'product.template'), ('res_id', '=', product.id), ('name', '=', file_ext )], limit=1 )
                    #if recs:
                    #    ir_att = request.env['ir.attachment'].browse(recs[0].id)
                    #    ir_att.sudo().write( dict( datas=base64.b64encode(cfe_file),datas_fname=values['tech_name']+'.'+file_ext ) )
                    #else:
                    try:
                        request.env['ir.attachment'].sudo().create(dict(
                                    datas=base64.b64encode(cfe_file),
                                    mimetype=file_ext,
                                    type='binary',
                                    name='module',
                                    datas_fname=values['tech_name']+'.'+file_ext,
                                    res_model='product.template',
                                    res_id=product.id,
                                    version=values['version'],
                                    version_description=values['version_description'],
                                    is_published=True,
                                ))
                    except UserError as e:
                        err_msg = e.name or e.value
                        if 'версия' in err_msg.lower():
                            error['version'] = err_msg
                            error_message.append( _('Укажите корректную версию') )
                        else:
                            error_message.append( err_msg )
                        values.update({'error': error, 'error_message': error_message})
                        response = request.render("moma.my_own_module", values)
                        response.headers['X-Frame-Options'] = 'DENY'
                        return response
            return request.redirect( redir )
        else: # not post
            product_public_categories = request.env['product.public.category'].sudo().search([], order='parent_id, id')
            prod_pub_cats = [dict(id=prod_pub_cat.id, name=self._complete_name_for_pubcat(prod_pub_cat)) for prod_pub_cat in product_public_categories]
            prod_attr = request.env['product.attribute'].search( [('name', '=', 'Для конфигурации')] , limit=1 )
            prod_attr_vals = request.env['product.attribute.value'].sudo().search( [ ('attribute_id','=',prod_attr[0].id)] , order='name' )
            all_confs = [dict(id=prod_attr_val.id, name=prod_attr_val.name) for prod_attr_val in prod_attr_vals]
            #_logger.info( "!!! all_confs=%s" % all_confs )
            if product:
                pub_cats = [dict(id=pub_cat.id, name=self._complete_name_for_pubcat(pub_cat)) for pub_cat in product.public_categ_ids]
                prod_attr_lines = request.env['product.template.attribute.line'].sudo().search( [('product_tmpl_id', '=', product.id), ('attribute_id','=',prod_attr[0].id) ] )
                #_logger.info( "!!! prod_attr_lines=%s" % prod_attr_lines )
                if prod_attr_lines :
                    prod_attr_values = prod_attr_lines[0].value_ids
                    #_logger.info( "!!! prod_attr_values=%s" % prod_attr_values )
                    for_confs = [dict(id=prod_attr_value.id, name=prod_attr_value.name) for prod_attr_value in prod_attr_values]
                else :
                    for_confs = []
                mmps = request.env['moma.module_privated'].search( [('module_id', '=', product.id) ] )
                #_logger.info( "!!! mmps=%s" % mmps )
                #for mmp in mmps:
                #    _logger.info( "!!! mmp=%s" % mmp )
                priv_partners = [dict(id=mmp.buyer_id.id, name=mmp.buyer_id.name) for mmp in mmps ]
                values.update({
                    'product': product,
                    #'pub_cats': ',,,'.join( str(x) for x in product.mapped('public_categ_ids').ids ), # id пубкатегорий продукта
                    'prod_pub_cats': json.dumps(prod_pub_cats),
                    'pub_cats': json.dumps(pub_cats),
                    'all_confs': json.dumps(all_confs),
                    'for_confs': json.dumps(for_confs),
                    'priv_partners': json.dumps(priv_partners),
                    'version': request.env['ir.attachment'].get_new_version(product.id),
                })
            else:
                values.update({
                    'product': None,
                    'prod_pub_cats': json.dumps(prod_pub_cats),
                    'all_confs': json.dumps(all_confs),
                    'mod_class': 1, # 1С расширение
                    'mod_type': 2, # Бесплатный
                    'version' : '1.0.0.0',
                })

        #countries = request.env['res.country'].sudo().search([])
        #states = request.env['res.country.state'].sudo().search([])

        #values.update({
        #    'partner': partner,
        #    'countries': countries,
        #    'states': states,
        #    'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
        #    'redirect': redirect,
        #    'page_name': 'my_own_modules',
        #})

        response = request.render("moma.my_own_module", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    # Не работает на апач ! Заработал только на порту 8069
    @http.route(['/my/own_modules/product/<int:product_id>/image'], type='http', auth="public", website=True, sitemap=False)
    def product_image(self, product_id, **post):
        status, headers, content = binary_content(model='product.template', id=product_id, field='image_medium', default_mimetype='image/png', env=request.env(user=SUPERUSER_ID))

        if not content:
            img_path = modules.get_module_resource('web', 'static/src/img', 'placeholder.png')
            with open(img_path, 'rb') as f:
                image = f.read()
            content = base64.b64encode(image)
        if status == 304:
            return werkzeug.wrappers.Response(status=304)
        image_base64 = base64.b64decode(content)
        headers.append(('Content-Length', len(image_base64)))
        response = request.make_response(image_base64, headers)
        response.status = str(status)
        return response

#    # Веб-категории для продукта
#    @http.route('/moma/get_pub_cats', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
#    def tag_read(self, q='', l=25, **post):
#        data = request.env['product.public.category'].search_read(
#            domain=[('name', '=ilike', (q or '') + "%")],
#            fields=['id', 'name'],
#            limit=int(l),
#        )
#        return json.dumps(data)

    def _get_search_domain_my_modules(self, search, attrib_values):
        domain = request.website.get_current_website().website_domain()

        partner = request.env.user.partner_id
        ids = []
        mmpus = request.env['moma.module_purchased'].search( [('buyer_id', '=', partner.id)] )
        #_logger.info( "!!! mmpus=%s" % mmpus )
        for mmpu in mmpus:
            #_logger.info( "!!! mmpu=%s" % mmpu )
            #_logger.info( "!!! mmpu.module_id=%s" % mmpu.module_id )
            #_logger.info( "!!! mmpu.module_id.id=%s" % mmpu.module_id.id )
            ids.append( mmpu.module_id.id )
        domain += [ ( 'id', 'in', ids ) ]

        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        #_logger.info( "!!! domain=%s" % domain )
        return domain

    @http.route([ '''/my/my_modules''','''/my/my_modules/page/<int:page>''' ], type='http', auth="user", website=True)
    def my_modules(self, page=0, search='', ppg=False, **post):
        category=None
        add_qty = int(post.get('add_qty', 1))

        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        attrib_set = {v[1] for v in attrib_values}

        domain = self._get_search_domain_my_modules(search, attrib_values)

        keep = QueryURL('/my/my_modules', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))

        pricelist_context, pricelist = self._get_pricelist_context()

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)

        url = "/my/my_modules"
        if search:
            post["search"] = search
        if attrib_list:
            post['attrib'] = attrib_list

        Product = request.env['product.template'].with_context(bin_size=True)

        search_categories = False
        search_product = Product.search(domain)

        product_count = len(search_product)
        pager = request.website.pager(url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
        products = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))

        ProductAttribute = request.env['product.attribute']
        if products:
            # get all products without limit
            attributes = ProductAttribute.search([('attribute_line_ids.value_ids', '!=', False), ('attribute_line_ids.product_tmpl_id', 'in', search_product.ids)])        
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        compute_currency = self._get_compute_currency(pricelist, products[:1])

        values = {
            'search': search,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'search_count': product_count,  # common for all searchbox
            'bins': TableCompute().process(products, ppg),
            'rows': PPR,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
        }
        return request.render("moma.my_modules", values)

    @http.route(['/my/my_module/<model("product.template"):product>'], type='http', auth="user", website=True)
    def my_module(self, product, search='', **kwargs):
        #if not product.can_access_from_current_website():
        #    raise NotFound()

        product_context = dict(request.env.context,
                               active_id=product.id,
                               partner=request.env.user.partner_id)

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attrib_set = {v[1] for v in attrib_values}

        keep = QueryURL('/my/my_modules', search=search, attrib=attrib_list)

        pricelist = request.website.get_current_pricelist()

        def compute_currency(price):
            return product.currency_id._convert(price, pricelist.currency_id, product._get_current_company(pricelist=pricelist, website=request.website), fields.Date.today())

        if not product_context.get('pricelist'):
            product_context['pricelist'] = pricelist.id
            product = product.with_context(product_context)

        values = {
            'search': search,
            #'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            # compute_currency deprecated, get from product
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            #'categories': categs,
            'main_object': product,
            'product': product,
            #'add_qty': add_qty,
            'optional_product_ids': [p.with_context({'active_id': p.id}) for p in product.optional_product_ids],
            # get_attribute_exclusions deprecated, use product method
            'get_attribute_exclusions': self._get_attribute_exclusions,
        }
        return request.render("moma.my_module", values)

    # Партнёры для приватных модулей
    @http.route('/moma/get_customers', type='http', auth="user", methods=['GET'], website=True, sitemap=False)
    def customers_read(self, q='', l=25, **post):
        data = request.env['res.partner'].search_read(
            domain=[ ('name', '=ilike', (q or '') + "%"), ('customer','=','True') ],
            fields=['id', 'name'],
            limit=int(l),
        )
        return json.dumps(data)

    #@http.route('/shop/payment/validate', type='http', auth="public", website=True)
    #def moma_payment_validate(self, transaction_id=None, sale_order_id=None, **post):
    #    """ Method that should be called by the server when receiving an update
    #    for a transaction. State at this point :

    #     - UDPATE ME
    #    """
    #    #_logger.info( "!!! moma_payment_validate) sale_order_id=%s" % sale_order_id )
    #    if sale_order_id is None:
    #        order = request.website.sale_get_order()
    #    else:
    #        order = request.env['sale.order'].sudo().browse(sale_order_id)
    #        assert order.id == request.session.get('sale_last_order_id')
    #    #_logger.info( "!!! 1) order=%s" % order )

    #    if transaction_id:
    #        tx = request.env['payment.transaction'].sudo().browse(transaction_id)
    #        assert tx in order.transaction_ids()
    #    elif order:
    #        tx = order.get_portal_last_transaction()
    #    else:
    #        tx = None

    #    if not order or (order.amount_total and not tx):
    #        return request.redirect('/shop')

    #    #_logger.info( "!!! 2) order.amount_total=%s" % order.amount_total )
    #    #_logger.info( "!!! 3) tx=%s" % tx )
    #    if order and not order.amount_total and not tx:
    #        order.sudo().write( {'state':'sale'} )
    #        for line in order.order_line:
    #            line.qty_to_invoice = line.product_uom_qty
    #        order.action_invoice_create()
    #        #_logger.info( "!!! 4) order.state=%s" % order.state )
    #        return request.redirect(order.get_portal_url())

    #    # clean context and session, then redirect to the confirmation page
    #    request.website.sale_reset()
    #    if tx and tx.state == 'draft':
    #        return request.redirect('/shop')

    #    PaymentProcessing.remove_payment_transaction(tx)
    #    return request.redirect('/shop/confirmation')
    
    # чтобы оформить заказ можно было только залогиненному
    @http.route(['/shop/checkout'], type='http', auth="user", website=True)
    def moma_checkout(self, **post):
        #the user is logged in to checkout
        return super(SaleController, self).checkout(**post)

    #@http.route(['/shop/confirmation'], type='http', auth="public", website=True)
    #def moma_payment_confirmation(self, **post):
    #    """ End of checkout process controller. Confirmation is basically seing
    #    the status of a sale.order. State at this point :

    #     - should not have any context / session info: clean them
    #     - take a sale.order id, because we request a sale.order and are not
    #       session dependant anymore
    #    """
    #    sale_order_id = request.session.get('sale_last_order_id')
    #    _logger.info( "!!! moma_payment_confirmation) sale_order_id=%s" % sale_order_id )
    #    if sale_order_id:
    #        _logger.info( "!!! 1) sale_order_id=%s" % sale_order_id )
    #        order = request.env['sale.order'].sudo().browse(sale_order_id)
    #        order.sudo().write( {status:'sale'} )
    #        _logger.info( "!!! 2) order.status=%s" % order.status )
    #        for line in order.order_line:
    #            line.qty_to_invoice = line.product_uom_qty
    #        retval = order.action_invoice_create()
    #        _logger.info( "!!! 3) order.action_invoice_create()=%s" % retval )
    #        return request.render("website_sale.confirmation", {'order': order})
    #    else:
    #        return request.redirect('/shop')

    def checkout_form_validate(self, mode, all_form_values, data):
        # mode: tuple ('new|edit', 'billing|shipping')
        # all_form_values: all values before preprocess
        # data: values after preprocess
        error = dict()
        error_message = []

        # Required fields from form
        required_fields = ['company_name','email']
        # error message for empty required fields
        for field_name in required_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'

        # email validation
        if data.get('email') and not tools.single_email_re.match(data.get('email')):
            error["email"] = 'error'
            error_message.append(_('Invalid Email! Please enter a valid email address.'))

        if [err for err in error.values() if err == 'missing']:
            error_message.append(_('Some required fields are empty.'))

        return error, error_message

    def values_postprocess(self, order, mode, values, errors, error_msg):
        new_values = {}
        authorized_fields = request.env['ir.model']._get('res.partner')._get_form_writable_fields()
        ###        
        fields_get = request.env['res.partner'].fields_get()
        authorized_fields['inn'] = fields_get['inn']
        authorized_fields['kpp'] = fields_get['kpp']
        ###
        for k, v in values.items():
            # don't drop empty value, it could be a field to reset
            if k in authorized_fields and v is not None:
                new_values[k] = v
            else:  # INFO ONLY
                if k not in ('field_required', 'partner_id', 'callback', 'submitted'): # classic case
                    _logger.info("website_sale postprocess: %s value has been dropped (empty or not writable)" % k)

        new_values['customer'] = True
        new_values['team_id'] = request.website.salesteam_id and request.website.salesteam_id.id
        new_values['user_id'] = request.website.salesperson_id and request.website.salesperson_id.id

        if request.website.specific_user_account:
            new_values['website_id'] = request.website.id

        if mode[0] == 'new':
            new_values['company_id'] = request.website.company_id.id

        lang = request.lang if request.lang in request.website.mapped('language_ids.code') else None
        if lang:
            new_values['lang'] = lang
        if mode == ('edit', 'billing') and order.partner_id.type == 'contact':
            new_values['type'] = 'other'
        if mode[1] == 'shipping':
            new_values['parent_id'] = order.partner_id.commercial_partner_id.id
            new_values['type'] = 'delivery'

        return new_values, errors, error_msg
