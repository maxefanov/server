# -*- coding: utf-8 -*-

import os.path
import base64
import json
import lxml
import requests
import werkzeug.exceptions
import werkzeug.urls
import werkzeug.wrappers

from datetime import datetime

from odoo import fields, http, tools, modules, SUPERUSER_ID, _
from odoo.http import content_disposition, Controller, request, route
from odoo.addons.web.controllers.main import binary_content
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager  # Import the classes

from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website.controllers.main import Website
from odoo.addons.sale.controllers.product_configurator import ProductConfiguratorController
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.osv import expression
from odoo.addons.website_sale.controllers.main import WebsiteSale  # Import the class
from odoo.addons.website_sale.controllers.main import TableCompute  # Import the class

from odoo.addons.website.controllers.main import Website  # Import the class

from odoo.addons.l10n_ru_doc.report_helper import QWebHelper

import logging
_logger = logging.getLogger(__name__)

PPG = 20  # Products Per Page
PPR = 4   # Products Per Row

class MomaWebsite(Website):

    # публикация только своих продуктов

    @http.route(['/website/publish'], type='json', auth="public", website=True)
    def moma_publish(self, id, object):
        Model = request.env[object]
        record = Model.browse(int(id))
        values = {}
        #_logger.info( "!!! record=%s" % record )
        #_logger.info( "!!! record.seller_id=%s" % record.seller_id )
        #_logger.info( "!!! request.env.user.partner_id=%s" % request.env.user.partner_id )
        if 'website_published' in Model._fields and record.seller_id.id==request.env.user.partner_id.id:
            values['website_published'] = not record.website_published
            record.sudo().write(values)

        return bool(record.website_published)
