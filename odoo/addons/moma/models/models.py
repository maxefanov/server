# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.http_routing.models import ir_http
from odoo.exceptions import UserError
from packaging import version
import re
import logging
_logger = logging.getLogger(__name__)

native_slugify_one = ir_http.slugify_one

def slugify_one(s,max_length=None):
# russian translit
	slovar = {'ъе':'ye','ый':'iy','ЪЕ':'YE','ЫЙ':'IY',
		'ьа':'ya','ьи':'yi','ье':'ye','ьо':'yo','ьу':'yu','ьы':'yy','ьэ':'ye',
		'ЬА':'YA','ЬИ':'YI','ЬЕ':'YE','ЬО':'YO','ЬУ':'YU','ЬЫ':'YY','ЬЭ':'YE',
		'а':'a','б':'b','в':'v','г':'g','д':'d','е':'e','ё':'yo','ж':'zh','з':'z', 
		'и':'i','й':'y','к':'k','л':'l','м':'m','н':'n','о':'o','п':'p','р':'r','с':'s', 
		'т':'t','у':'u','ф':'f','х':'kh','ц':'ts','ч':'ch','ш':'sh','щ':'sch','ъ':'', 
		'ы':'y','ь':'','э':'e','ю':'yu','я':'ya', 'А':'A','Б':'B','В':'V','Г':'G','Д':'D', 
		'Е':'E','Ё':'Yo','Ж':'Zh','З':'Z','И':'I','Й':'Y','К':'K','Л':'L','М':'M','Н':'N', 
		'О':'O','П':'P','Р':'R','С':'S','Т':'T','У':'U','Ф':'F','Х':'Kh','Ц':'Ts','Ч':'Ch', 
		'Ш':'Sh','Щ':'Sch','Ъ':'','Ы':'Y','Ь':'','Э':'E','Ю':'Yu','Я':'Ya'}
	for key in slovar:
		s = s.replace(key, slovar[key])
	return native_slugify_one(s,max_length)
	
ir_http.slugify_one = slugify_one

partner_types = [ (0,'undefined'), (1,'Физическое лицо'), (2,'Самозанятый'), (3,'Индивидуальный Предприниматель'), (4,'Юридическое лицо'), ]


class ResPartner(models.Model):
    _inherit = 'res.partner'

    partner_type = fields.Selection(partner_types,string="Тип партнёра",required=False,default=0) # required=True
    web_site = fields.Char(string="Сайт",size=100,required=False)

    #@api.model
    #def create(self, vals):
    #    res_partner = super(ResPartner, self).create(vals)
    #    if( res_partner.partner_type==4 ):
    #        res_partner.is_company = True
    #    else:
    #        res_partner.is_company = False
    #    return res

    #@api.multi
    #def write(self, values):
    #    _logger.info( "!!! get('partner_type')=%s" % get('partner_type') )
    #    res = super(ResPartner, self).write(values)
    #    for res_partner in self:
    #        _logger.info( "!!! res_partner.partner_type=%s" % res_partner.partner_type )
    #        #if( res_partner.partner_type==4 ):
    #        #    res_partner.is_company = True
    #        #else:
    #        #    res_partner.is_company = False
    #    return res

module_classes = [ (0,'undefined'), (1,'1С расширение'), (2,'1С обработка'), ]
module_types = [ (0,'undefined'), (1,'Приватный'), (2,'Бесплатный'), (3,'Платный'), ]

# Продукт как модуль
class ProductTemplate(models.Model):
    _inherit = 'product.template'
    _name = 'product.template'

    mod_class = fields.Selection(module_classes,string="Вид модуля",required=True,default=0) # required=True
    mod_type = fields.Selection(module_types,string="Тип модуля",required=True,default=0) # required=True
    tech_name = fields.Char(string="Тех. наименование",size=50,required=True) # required=True
    seller_id = fields.Many2one("res.partner","Разработчик",required=True) # required=True
    forum_post_id = fields.Many2one("forum.post","Описание модуля на форуме")
    blog_post_id = fields.Many2one("blog.post","Описание модуля в блоге")
    module_privated_ids = fields.Many2many('moma.module_privated', string='Приватные модули')
    module_purchased_ids = fields.Many2many('moma.module_purchased', string='Заказанные модули')

    _sql_constraints = [
        ('tech_name_unique', 'unique(tech_name)', 'Такой технический синоним уже есть !')
        ]

    def _write_attr_mod_type(self,res):
        #_logger.info( "!!! res.mod_class=%s" % res.mod_class )
        if res.mod_class > 0 :
            str_mod_class = module_classes[res.mod_class][1];
            #_logger.info( "!!! str_mod_class=%s" % str_mod_class ) #!!!
            recs = self.env['product.attribute'].search( [('name', '=', 'Вид')] , limit=1 )
            if recs:
                #_logger.info( "!!! product.attribute.id(name='Вид') = %s" % recs[0].id ) #!!!
                att_type_vid_id = recs[0].id #= self.env['product.attribute'].browse(recs[0].id)
                #_logger.info( "!!! att_type_vid_id=%s" % att_type_vid_id ) #!!!
                recs = self.env['product.attribute.value'].sudo().search( [ ('name','=',str_mod_class), ('attribute_id','=',att_type_vid_id)] , limit=1 )
                #recs1 = self.env['product.attribute.value'].sudo().search( [ ('name','=',str_mod_class) ] , limit=10 )
                #recs2 = self.env['product.attribute.value'].sudo().search( [ ('attribute_id','=',att_type_vid_id)] , limit=10 )
                #_logger.info( "!!! recs=%s" % recs ) #!!!
                #_logger.info( "!!! recs1=%s" % recs1 ) #!!!
                #_logger.info( "!!! recs2=%s" % recs2 ) #!!!
                #prod_attr_val1 = self.env['product.attribute.value'].browse(recs2[1].id)
                #_logger.info( "!!! prod_attr_val1.name = %s" % prod_attr_val1.name ) #!!!
                #prod_attr_val2 = self.env['product.attribute.value'].browse(recs2[2].id)
                #_logger.info( "!!! prod_attr_val2.name = %s" % prod_attr_val2.name ) #!!!
                prod_attr_val = self.env['product.attribute.value'].browse(recs[0].id)
                prod_attr_val_id = prod_attr_val.id
                mod_classes = [ prod_attr_val_id, ]
                recs = self.env['product.template.attribute.line'].search( [('product_tmpl_id', '=', res.id), ('attribute_id','=',att_type_vid_id) ] , limit=1 )
                if recs:
                    ptal = self.env['product.template.attribute.line'].browse(recs[0].id)
                    ptal.write({
                        'value_ids': [[6, False, mod_classes]],
                    })
                else:
                    self.env['product.template.attribute.line'].create({
                        'product_tmpl_id': res.id,
                        'attribute_id': att_type_vid_id,
                        'value_ids': [[6, False, mod_classes]],
                    })
        if res.mod_type!=0 or res.mod_type!=3 : # определён и не приватный
            str_mod_type = module_types[res.mod_type][1];
            recs = self.env['product.attribute'].search( [('name', '=', 'Тип')] , limit=1 )
            if recs:
                att_type = self.env['product.attribute'].browse(recs[0].id)
                recs = self.env['product.attribute.value'].search( [('attribute_id','=',att_type.id), ('name', '=', str_mod_type)] , limit=1 )
                mod_types = [ self.env['product.attribute.value'].browse(recs[0].id).id, ]
                recs = self.env['product.template.attribute.line'].search( [('product_tmpl_id', '=', res.id), ('attribute_id','=',att_type.id) ] , limit=1 )
                if recs:
                    ptal = self.env['product.template.attribute.line'].browse(recs[0].id)
                    ptal.write({
                        'value_ids': [[6, False, mod_types]],
                    })
                else:
                    self.env['product.template.attribute.line'].create({
                        'product_tmpl_id': res.id,
                        'attribute_id': att_type.id,
                        'value_ids': [[6, False, mod_types]],
                    })

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        self._write_attr_mod_type(res)
        return res

    @api.multi
    def write(self, values):
        res = super(ProductTemplate, self).write(values)
        for prod_templ in self:
            self._write_attr_mod_type(prod_templ)
        return res


class MomaModulePrivated(models.Model):
    _name = 'moma.module_privated'
    _description = "Приватные модули"

    module_id = fields.Many2one("product.template","Модуль")
    buyer_id = fields.Many2one("res.partner","Покупатель")

    def _write_to_purchased(self,res):
        cnt = self.env['moma.module_purchased'].search_count( [('module_id', '=', res.module_id.id), ('buyer_id','=',res.buyer_id.id) ] )
        if cnt>0:
            recs = self.env['moma.module_purchased'].search( [('module_id', '=', res.module_id.id), ('buyer_id','=',res.buyer_id.id) ] )
            for rec in recs:
                mmp = self.env['moma.module_purchased'].browse(rec.id)
                mmp.write({
                    'module_id': res.module_id.id,
                    'buyer_id': res.buyer_id.id,
                })
        else:
            self.env['moma.module_purchased'].create({
                            'module_id': res.module_id.id,
                            'buyer_id': res.buyer_id.id,
                        })

    @api.model
    def create(self, vals):
        res = super(MomaModulePrivated, self).create(vals)
        self._write_to_purchased(res)
        return res

    @api.multi
    def write(self, values):
        res = super(MomaModulePrivated, self).write(values)
        for mmp in self:
            self._write_to_purchased(mmp)
        return res

    @api.multi
    def unlink(self):
        recs = self.env['moma.module_purchased'].search( [('module_id', '=', self.module_id.id), ('buyer_id','=',self.buyer_id.id) ] )
        recs.unlink()
        return super(MomaModulePrivated, self).unlink()


class MomaModulePurchased(models.Model):
    _name = 'moma.module_purchased'
    _description = "Заказанные модули" # в том числе, приватные и купленные

    module_id = fields.Many2one("product.template","Модуль")
    buyer_id = fields.Many2one("res.partner","Использующий")
    date_of_expire = fields.Date(string="Дата окончания использования") 


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _write_to_purchased(self,res):
        for line in self.order_line:
            module_id = line.product_id.product_tmpl_id.id
            cnt = self.env['moma.module_purchased'].search_count( [('module_id', '=', module_id), ('buyer_id','=',res.partner_id.id) ] )
            if cnt>0 :
                recs = self.env['moma.module_purchased'].search( [('module_id', '=', module_id), ('buyer_id','=',res.partner_id.id) ] )
                for rec in recs:
                    mmp = self.env['moma.module_purchased'].browse(rec.id)
                    if self.state=='cancel' :
                        mmp.unlink() # sale order отменён, удалить запись
            elif self.state!='cancel' and line.price_total==0 :
                # sale order не отменён и модуль бесплатный
                self.env['moma.module_purchased'].create({
                                'module_id': module_id,
                                'buyer_id': res.partner_id.id,
                            })

    @api.multi
    def write(self, values):
        res = super(SaleOrder, self).write(values)
        for sal_ord in self:
            self._write_to_purchased(sal_ord)
        return res


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _write_to_purchased(self,res):
        for line in self.invoice_line_ids:
            module_id = line.product_id.product_tmpl_id.id
            cnt = self.env['moma.module_purchased'].search_count( [('module_id', '=', module_id), ('buyer_id','=',res.partner_id.id) ] )
            if cnt>0 :
                recs = self.env['moma.module_purchased'].search( [('module_id', '=', module_id), ('buyer_id','=',res.partner_id.id) ] )
                for rec in recs:
                    mmp = self.env['moma.module_purchased'].browse(rec.id)
                    if self.state=='cancel' and line.price_total!=0 :
                        mmp.unlink() # инвойс отменён, для платного модуля удалить запись
            elif self.state!='cancel' and ( line.price_total==0 or self.state=='paid' ) :
                # счёт не отменён + модуль бесплатный или счёт оплачен
                self.env['moma.module_purchased'].create({
                                'module_id': module_id,
                                'buyer_id': res.partner_id.id,
                            })

    @api.multi
    def write(self, values):
        res = super(AccountInvoice, self).write(values)
        for acc_inv in self:
            self._write_to_purchased(acc_inv)
        return res

    @api.multi
    def action_invoice_open(self):
        # OVERRIDE
        # Auto-reconcile the invoice with payments coming from transactions.
        # It's useful when you have a "paid" sale order (using a payment transaction) and you invoice it later.
        res = super(AccountInvoice, self).action_invoice_open()

        if not self:
            return res

        for invoice in self:
            payments = invoice.mapped('transaction_ids.payment_id')
            _logger.info("action_invoice_open payments=%s",payments)
            move_lines = payments.mapped('move_line_ids').filtered(lambda line: not line.reconciled and line.credit > 0.0)
            for line in move_lines:
                invoice.assign_outstanding_credit(line.id)
        return res


class MomaModuleInUsage(models.Model):
    _name = 'moma.module_inusage'
    _description = "Используемые модули"
#    _order = 'name'

    module_id = fields.Many2one("product.template","Модуль")
    infobase_id = fields.Many2one("moma.infobase","Информационная база")


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'
#    _name = 'ir.attachment'

    version = fields.Char('Версия')
    is_published = fields.Boolean('Версия опубликована')
    version_description = fields.Char('Описание для версии')
    download_count = fields.Integer(string='Скачиваний', compute='_compute_download_count')

    @api.depends('res_model', 'name', 'res_id', 'version')
    def _compute_download_count(self):
        for attach in self:
            attach.download_count = 0
            if attach.res_model=="product.template" and attach.name=="module":
                if attach.version != '':
                    mas_recs = self.env['moma.attachment_stat'].search( [ ('attachment_id', '=', attach.id) ] )
                    for mas_rec in mas_recs:
                        attach.download_count = attach.download_count + mas_rec.download_count

    def check_version(self,vers):
        try:
            v = version.Version(vers)
            return True
        except version.InvalidVersion:
            return False
        except TypeError:
            return False
        #match = re.fullmatch(r'^[0-9][0-9.]*$',vers)
        #if match:
        #    return True
        #else:
        #    return False

    def get_versions_list(self,product_id):
        recs_att = self.env['ir.attachment'].search_read( [('res_model', '=', 'product.template'), ('name', '=', 'module'), ('res_id','=',product_id) ],order="create_date" )
        return [d['version'] for d in recs_att if 'version' in d]

    def get_last_version(self,product_id):
        vers_list = self.get_versions_list(product_id)
        if not vers_list:
            return False
        else:
            return vers_list[-1]

    def get_new_version(self,product_id):
        vers_last = self.get_last_version(product_id)
        if not vers_last:
            return '1.0.0.0'
        ver_nums_list = list(map(int, (vers_last.split("."))))
        ver_nums_list[-1] = ver_nums_list[-1] + 1
        return '.'.join( map(str,ver_nums_list) )

    @api.model
    def create(self, vals):
        #_logger.info("IrAttachment.create vals=%s",vals)
        if vals.get('res_model')=='product.template' and vals.get('name')=='module':
            if not self.check_version( vals.get('version') ):
                raise UserError("Неверно задана версия модуля.")
            else:
                last_ver = self.get_last_version( vals.get('res_id') )
                if last_ver and version.Version( vals.get('version') ) < version.Version(last_ver):
                    raise UserError("Заданная версия модуля не больше, чем уже имеющаяся последняя версия.")
        res = super(IrAttachment, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if vals.get('version'):
            if not self.check_version(vals.get('version')):
                raise UserError("Неверно задана версия модуля.")
            else:
                for ir_att in self:
                    recs_att = self.env['ir.attachment'].search_read( [('res_model', '=', 'product.template'), ('res_id','=',ir_att.res_id), ('name', '=', 'module') ], order="create_date" )
                    _logger.info("1) IrAttachment.write recs_att=%s",recs_att)
                    id_list = [d['id'] for d in recs_att if 'id' in d]
                    _logger.info("2) IrAttachment.write id_list=%s",id_list)
                    version_list = [d['version'] for d in recs_att if 'version' in d]
                    _logger.info("3) IrAttachment.write version_list=%s",version_list)
                    idx = id_list.index( ir_att.id )
                    _logger.info("4) IrAttachment.write idx=%s",idx)
                    if idx > 0:
                        if version.Version(vals.get('version')) <= version.Version(version_list[idx-1]) :
                            raise UserError("Версия модуля не больше, чем предыдущая.")
                            break
                    if idx < len(id_list)-1:
                        if version.Version(vals.get('version')) >= version.Version(version_list[idx+1]) :
                            raise UserError("Версия файла не меньше, чем последующая.")
                            break
        else:
            for ir_att in self:
                if ir_att.res_model=='product.template' and ir_att.name=='module' : # версия должна быть
                    raise UserError("Не задана версия модуля.")

        res = super(IrAttachment, self).write(vals)
        return res


class MomaAttachmentStat(models.Model):
    _name = 'moma.attachment_stat'
    _description = "Статистика скачиваний"

    attachment_id = fields.Many2one("ir.attachment","Аттачмент",index=True)
    buyer_id = fields.Many2one("res.partner","Покупатель",index=True)
    download_count = fields.Integer(string="Количество скачиваний",required=True,default=0) 

    #def add_download( self,att_id, buy_id ):
    #    mas_recs = self.search_read( [('attachment_id', '=', att_id), ('buyer_id','=',buy_id) ] )
    #    if mas_recs.len()==0:
    #        self.create( {'attachment_id':att_id, 'buyer_id':buy_id, 'download_count':1} )
    #    else:
    #        for mas_rec in mas_recs:
    #            dwnc = mas_rec.download_count
    #            mas = self.browse(mas_rec.id)
    #            mas.write( { 'download_count': dwnc+1, } )

class MomaConfiguration(models.Model):
    _name = 'moma.configuration'
    _description = "Конфигурации 1С"
    _order = 'tech_name,name'

    name = fields.Char(string="Наименование конфигурации",size=200,required=True)
    tech_name = fields.Char(string="Тех. наименование конфигурации",size=100,required=True)
    desc = fields.Text(string="Описание конфигурации") 


class MomaInfobase(models.Model):
    _name = 'moma.infobase'
    _description = "Информационные базы"
    _order = 'name,tech_name'

    name = fields.Char(string="Наименование инфобазы",size=200,required=True)
    tech_name = fields.Char(string="Тех. наименование инфобазы",size=100,required=True)
    desc = fields.Text(string="Описание инфобазы") 

    owner_id = fields.Many2one("res.partner","Владелец инфобазы",index=True,required=True)

    platform_version = fields.Char(string="Версия платформы",required=True)

    config_id = fields.Many2one("moma.configuration","Конфигурация 1С",index=True,required=True)
    config_name = fields.Char(related='config_id.name', store=True, readonly=False)
    config_tech_name = fields.Char(related='config_id.tech_name', store=True, readonly=False)
    config_desc = fields.Text(related='config_id.desc', store=True, readonly=False)
    config_version = fields.Char(string="Версия конфигурации",required=True)

    @api.model
    def create(self, vals):
        #_logger.info("IrAttachment.create vals=%s",vals)
        if vals.get('config_name') and vals.get('config_tech_name') and not vals.get('config_id') :
            recs_conf = self.env['moma.configuration'].search( [('name', '=', vals.get('config_name')), ('tech_name','=',vals.get('config_tech_name')) ], limit=1 )
            if not recs_conf :
                recs_conf = self.env['moma.configuration'].create({
                    'name': vals.get('config_name'),
                    'tech_name': vals.get('config_tech_name'),
                    })
            vals.update( {'config_id' : rec_conf[0].id} )
        if vals.get('config_name') :
            del vals['config_name']
        if vals.get('config_tech_name') :
            del vals['config_tech_name']
        res = super(MomaInfobase, self).create(vals)
        return res


class MomaAttachmentDownloadsLog(models.Model):
    _name = 'moma.attachment_downloads_log'
    _description = "Лог скачиваний"

    attachment_id = fields.Many2one("ir.attachment","Аттачмент",index=True,required=True)
    buyer_id = fields.Many2one("res.partner","Покупатель",index=True,required=True)
    infobase_id = fields.Many2one("moma.infobase","Информационная база",index=True,required=True)

    infobase_name = fields.Char(related='infobase_id.name', store=True, readonly=False)
    infobase_tech_name = fields.Char(related='infobase_id.tech_name', store=True, readonly=False)
    infobase_platform_version = fields.Char(related='infobase_id.platform_version', store=True, readonly=False)
    infobase_config_version = fields.Char(related='infobase_id.config_version', store=True, readonly=False)
    
    infobase_config_name = fields.Char(related='infobase_id.config_id.name', store=True, readonly=False)
    infobase_config_tech_name = fields.Char(related='infobase_id.config_id.tech_name', store=True, readonly=False)

    @api.model
    def create(self, vals):
        partner_id = self.env.user.partner_id;
        if partner_id==vals.buyer_id:
        # записывать можно только свои скачивания
            if not vals.get('infobase_id') :
                recs_conf = self.env['moma.infobase'].search( [('name', '=', vals.get('infobase_name')), ('tech_name','=',vals.get('infobase_tech_name')),
                                                              ('platform_version', '=', vals.get('infobase_platform_version')), ('config_version','=',vals.get('infobase_config_version')),
                                                              ('config_name', '=', vals.get('infobase_config_name')), ('config_tech_name','=',vals.get('infobase_config_tech_name')),
                                                              ('owner_id', '=', vals.get('buyer_id'))], limit=1 )
                if not recs_conf :
                    recs_conf = self.env['moma.infobase'].create({
                        'name': vals.get('infobase_name'),
                        'tech_name': vals.get('infobase_tech_name'),
                        'platform_version': vals.get('infobase_platform_version'),
                        'config_version': vals.get('infobase_config_version'),
                        'config_name': vals.get('infobase_config_name'),
                        'config_tech_name': vals.get('infobase_config_tech_name'),
                        'owner_id': vals.get('buyer_id'),
                        })
                vals.update( {'infobase_id' : rec_conf[0].id} )
            if vals.get('infobase_name') :
                del vals['infobase_name']
            if vals.get('infobase_tech_name') :
                del vals['infobase_tech_name']
            if vals.get('infobase_platform_version') :
                del vals['infobase_platform_version']
            if vals.get('infobase_config_version') :
                del vals['infobase_config_version']
            if vals.get('infobase_config_name') :
                del vals['infobase_config_name']
            if vals.get('infobase_config_tech_name') :
                del vals['infobase_config_tech_name']
            res = super(MomaAttachmentDownloadsLog, self).create(vals)
            recs = self.env['ir.attachment_stat'].search( [ ('attachment_id', '=', vals.attachment_id), ('buyer_id', '=', vals.buyer_id) ] , limit=1 )
            if recs:
                att_stat = self.env['ir.attachment_stat'].browse( recs[0].id )
                att_stat.write( { 'download_count': att_stat.download_count+1, } )
        return res
