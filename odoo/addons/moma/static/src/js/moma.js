odoo.define('moma.moma', function (require) {
    'use strict';

    require('web.dom_ready');
    var ajax = require('web.ajax');
    var core = require('web.core');

    var _t = core._t;

    var lastsearch;

    if (!$('.moma').length) {
        return $.Deferred().reject("DOM doesn't contain '.moma'");
    }

    $('.o_moma_profile_pic_edit').on('click', function (ev) {
        ev.preventDefault();
        $(this).closest('form').find('.o_moma_file_upload').trigger('click');
    });

    $('.o_moma_file_upload').on('change', function () {
        if (this.files.length) {
            var $form = $(this).closest('form');
            var reader = new window.FileReader();
            reader.onload = function (ev) {
                $form.find('.o_moma_product_image_big').attr('src', ev.target.result);
            };
            reader.readAsDataURL(this.files[0]);
            $form.find('#moma_clear_image').remove();
        }
    });

    $('.o_moma_profile_pic_clear').click(function () {
        var $form = $(this).closest('form');
        $form.find('.o_moma_product_image_big').attr("src", "/web/static/src/img/placeholder.png");
        $form.append($('<input/>', {
            name: 'clear_image',
            id: 'moma_clear_image',
            type: 'hidden',
        }));
    });

    $('input.js_moma_pubcat_select2').select2({
        minimumInputLength: 0,
        multiple: true,
        maximumSelectionSize: 10,
        allowClear: true,
        // Take all options from the input value
        data: function () {
            var all_vals = [];
            var element = $('input.js_moma_pubcat_select2');
            _.each(element.data('all-values'), function (x) {
                all_vals.push({ id: x.id, text: x.name });
            });
            return { results: all_vals }
        },
        // Take default options from the input value
        initSelection: function (element, callback) {
            var sel_vals = [];
            _.each(element.data('init-value'), function (x) {
                sel_vals.push({ id: x.id, text: x.name, selected: true });
            });
            element.val('');
            callback(sel_vals);
        },
    });

    $('input.js_moma_for_conf_select2').select2({
        minimumInputLength: 0,
        multiple: true,
        maximumSelectionSize: 10,
        allowClear: true,
        // Take all options from the input value
        data: function () {
            var all_vals = [];
            var element = $('input.js_moma_for_conf_select2');
            _.each(element.data('all-values'), function (x) {
                all_vals.push({ id: x.id, text: x.name });
            });
            return { results: all_vals }
        },
        // Take default options from the input value
        initSelection: function (element, callback) {
            var sel_vals = [];
            _.each(element.data('init-value'), function (x) {
                sel_vals.push({ id: x.id, text: x.name, selected: true });
            });
            element.val('');
            callback(sel_vals);
        },
    });

    $('input.js_moma_priv_partners_select2').select2({
        tags: true,
        tokenSeparators: [",", " ", "_"],
        maximumInputLength: 35,
        minimumInputLength: 2,
        maximumSelectionSize: 5,
        lastsearch: [],
        createSearchChoice: function (term) {
            if ($(lastsearch).filter(function () { return this.text.localeCompare(term) === 0; }).length === 0) {
                return {
                    id: "_" + $.trim(term),
                    text: $.trim(term) + ' *',
                    isNew: true,
                };
                //}
            }
        },
        formatResult: function (term) {
            if (term.isNew) {
                return '';
            }
            else {
                return _.escape(term.text);
            }
        },
        ajax: {
            url: '/moma/get_customers',
            dataType: 'json',
            data: function (term) {
                return {
                    q: term,
                    l: 50
                };
            },
            results: function (data) {
                var ret = [];
                _.each(data, function (x) {
                    ret.push({ id: x.id, text: x.name, isNew: false });
                });
                lastsearch = ret;
                return { results: ret };
            }
        },
        // Take default tags from the input value
        initSelection: function (element, callback) {
            var data = [];
            _.each(element.data('init-value'), function (x) {
                data.push({ id: x.id, text: x.name, isNew: false });
            });
            element.val('');
            callback(data);
        },
    });

});
