# -*- coding: utf-8 -*-
{
    'name': "ModuleMarket v.2",

    'summary': """Module Market functionality for odoo v.2""",

    'description': """Module Market functionality for odoo v.2""",

    'author': "Maxim Efanov",
    'website': "http://maxim-efanov.ru",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Website',
    'version': '12.0.2.0',

    # any module necessary for this one to work correctly
    'depends': ['base','web','portal','website','website_blog','website_sale'],
    'external_dependencies': {
        'python': [
        ],
    },
	
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/moma.xml',
        'views/moma_portal_template.xml',
        'views/moma_template.xml',
        'views/moma_sale_order_template.xml',
        'views/moma_template4users.xml',
        'views/moma_menu.xml',
        'views/moma_view.xml',
        'views/moma_website_sale_template.xml',
    ],
	# qWeb
    #'qweb': [
	#	'static/src/xml/icash_button.xml',
    #],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}