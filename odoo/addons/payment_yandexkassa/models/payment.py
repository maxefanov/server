# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import hashlib

from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.tools.float_utils import float_compare

from yandex_checkout import Configuration, Payment
import uuid

import logging

_logger = logging.getLogger(__name__)


class PaymentAcquirerYandexKassa(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('yandexkassa', 'YandexKassa')])
    yandexkassa_shop_id = fields.Char(string='Shop id', required_if_provider='yandexkassa', groups='base.group_user')
    yandexkassa_api_key = fields.Char(string='API key', required_if_provider='yandexkassa', groups='base.group_user')

    @api.multi
    def yandexkassa_form_generate_values(self, values):
        self.ensure_one()
        # передаём данные
        yandexkassa_values = dict(values,
            acquirer_id=self.id,
            description=values['reference'],
                                )
        return yandexkassa_values

    @api.multi
    def yandexkassa_get_form_action_url(self):
        self.ensure_one()
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        return urls.url_join(base_url, '/payment/yandexkassa/redirect')


class PaymentTransactionYandexKassa(models.Model):
    _inherit = 'payment.transaction'

    yk_payment_id = fields.Char(string='YandexKassa payment id', required_if_provider='yandexkassa', groups='base.group_user')

    @api.model
    def _yandexkassa_form_get_tx_from_data(self, data):
        """ Given a data dict coming from yandex kassa, verify it and find the related
        transaction record. """
        reference = data.get('reference')
        if not reference:
            raise ValidationError(_('YandexKassa: received data with missing reference %s') % (reference))

        transaction = self.search([('reference', '=', reference)])

        if not transaction:
            error_msg = (_('YandexKassa: received data for reference %s; no order found') % (reference))
            raise ValidationError(error_msg)
        elif len(transaction) > 1:
            error_msg = (_('YandexKassa: received data for reference %s; multiple orders found') % (reference))
            raise ValidationError(error_msg)

        return transaction

    @api.multi
    def _yandexkassa_form_get_invalid_parameters(self, data):
        invalid_parameters = []
        _logger.info("yandexkassa_form_get_invalid_parameters() data=%s",data)
        #check what is buyed
        if float_compare(float(data.get('amount', '0.0')), self.amount, 2) != 0:
            invalid_parameters.append(
                ('Amount', data.get('amount'), '%.2f' % self.amount))

        return invalid_parameters

    @api.multi
    def _yandexkassa_form_validate(self, data):
        _logger.info("yandexkassa_form_validate() data=%s",data)
        status = data.get('status')
        result = self.write({
            'yk_payment_id': data.get('yk_payment_id'),
            'date': fields.Datetime.now(),
        })
        if status == 'succeeded':
            self._set_transaction_done()
        elif status != 'pending':
            self._set_transaction_cancel()
        else:
            self._set_transaction_pending()
        return result
