# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'YandexKassa Payment Acquirer',
    'category': 'Payment Acquirer',
    'summary': 'Payment Acquirer: YandexKassa Implementation',
    'description': """
    YandexKassa Payment Acquirer for Russia.

    YandexKassa payment gateway supports only RUB currency.
    """,
    'depends': ['payment'],
    'data': [
        'views/payment_views.xml',
        'views/payment_yandexkassa_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'post_init_hook': 'create_missing_journal_for_acquirers',
}
