# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import pprint
import werkzeug

from odoo import http
from odoo.http import request
from werkzeug import urls
from yandex_checkout import Configuration, Payment, Webhook
import uuid

_logger = logging.getLogger(__name__)


class YandexKassaController(http.Controller):

    @http.route(['/payment/yandexkassa/notification'], type='http', auth='public', csrf=False)
    def yandexkassa_notification(self, **post):
        _logger.info(
            'YandexKassa notification : with post data %s', pprint.pformat(post))
        #if post:
        #        request.env['payment.transaction'].sudo().form_feedback(post, 'yandexkassa')
        return {'shop_status': 'ok'}

    @http.route(['/payment/yandexkassa/returl'], type='http', auth='public', csrf=False)
    def yandexkassa_returl(self, **post):
        _logger.info(
            'YandexKassa yandexkassa_returl : with post data %s', pprint.pformat(post))
        if post:
            if post['ref']:
                data = { 'reference':post['ref'] }
                tx = request.env['payment.transaction'].sudo()._yandexkassa_form_get_tx_from_data(data)
                payment_acquirer = request.env['payment.acquirer'].sudo().browse( int( tx.acquirer_id ) )
                Configuration.account_id = payment_acquirer.yandexkassa_shop_id
                Configuration.secret_key = payment_acquirer.yandexkassa_api_key
                yk_payment = Payment.find_one(tx.yk_payment_id)
                data['yk_payment_id'] = yk_payment.id
                data['status'] = yk_payment.status
                data['amount'] = yk_payment.amount.value
                request.env['payment.transaction'].sudo().form_feedback(data, 'yandexkassa')
        return werkzeug.utils.redirect('/payment/process')

    @http.route(['/payment/yandexkassa/redirect'], type='http', auth='public', csrf=False)
    def yandexkassa_redirect(self, **post):
        _logger.info(
            'YandexKassa redirect : redirect with post data %s', pprint.pformat(post))

        if post:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            _logger.info( 'web.base.url=%s',base_url )
            ret_url = urls.url_join(base_url, '/payment/yandexkassa/returl?ref=')
            ret_url += post['reference'];
            _logger.info( 'ret_url=%s',ret_url )
            payment_acquirer = request.env['payment.acquirer'].sudo().browse( int(post.get('acquirer_id')) )
            Configuration.account_id = payment_acquirer.yandexkassa_shop_id
            Configuration.secret_key = payment_acquirer.yandexkassa_api_key
            #Configuration.configure_auth_token('<Bearer Token>')
            #response = Webhook.add({
            #    "event": "payment.succeeded",
            #    "url": "https://module-app.store/payment/yandexkassa/notification",
            #})
            #response = Webhook.add({
            #    "event": "refund.succeeded",
            #    "url": "https://module-app.store/payment/yandexkassa/notification",
            #})
            #response = Webhook.add({
            #    "event": "payment.canceled",
            #    "url": "https://module-app.store/payment/yandexkassa/notification",
            #})
            idempotence_key = str(uuid.uuid4())
            yk_payment = Payment.create({
                "amount": {
                    "value": post['amount'],
                    "currency": "RUB"
                },
                "confirmation": {
                    "type": "redirect",
                    "return_url": ret_url
                },
                "capture": True,
                "metadata": {
                    "reference": post['reference'],
                    "partner_email": post['partner_email'],
                    "partner_phone": post['partner_phone']
                },
                "description": post['reference']
            }, idempotence_key)
            _logger.info( 'yk_payment.id = %s', yk_payment.id )
            _logger.info( 'yk_payment.confirmation_url = %s', yk_payment.confirmation.confirmation_url )
            _logger.info( 'yk_payment = %s', yk_payment )
            data = { 'reference': post['reference'] }
            tx = request.env['payment.transaction'].sudo()._yandexkassa_form_get_tx_from_data( data )
            tx.write( {'yk_payment_id':yk_payment.id} )
            return werkzeug.utils.redirect(yk_payment.confirmation.confirmation_url)

        return werkzeug.utils.redirect('/404')
